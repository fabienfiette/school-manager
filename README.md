# School Manager

## Installation

Use the package manager [npm](https://www.npmjs.com/) to install helloworld.

Use node v16 use :
```bash
nvm install 16
```

```bash
npm i
```

## Usage

Define `url` of api in `src/index.js` `line 10` `this.urlApi = 'define url here'`

Define `x-api-key` in `src/components/pages/SignUp.js` `line 62` `myHeader.append('x-api-key', 'define key here');`

Start the application dev with :

```bash
npm run start
```

Created the dist with :

```bash
npm run dist
```

Analyse the coding rules with :

```bash
npm run lint
```

# AWS API Documentation

This documention list all table avaible, whith request for each table;

## school-manager-users

### GET all users

`GET url?table=school-manager-users`

#### Response

    Date: Wed, 11 May 2022 16:50:27 GMT
    Status: 200 OK
    Connection: keep-alive
    Content-Type: application/json
    Content-Length: 999

    [
        {
            "password": "kBE8rU9aJ6BQ@uQ",
            "last_name": "Fiette",
            "first_name": "Fabien",
            "id_promo": "1",
            "id_speciality": "1",
            "id": "x771440307e085d7dee3c",
            "email": "fabienfiette@hotmail.fr",
            "gender": true,
            "age": "20"
        },
        {
            "password": "ivioezvoizepmvjaemvjm",
            "last_name": "Le mec la",
            "first_name": "Sylvain",
            "id_promo": "1",
            "id_speciality": "1",
            "id": "x1270ae8ee1fb7d863899",
            "email": "sylvainlemecla@gmail.com",
            "gender": false,
            "age": "28"
        }
    ]

### GET with details

`GET url?table=school-manager-users&{column_name}={condition}`
###### Example : `GET url?table=school-manager-users&id=x771440307e085d7dee3c`
#### Response

    Date: Wed, 11 May 2022 16:50:27 GMT
    Status: 200 OK
    Connection: keep-alive
    Content-Type: application/json
    Content-Length: 815

    [
        {
            "password": "kBE8rU9aJ6BQ@uQ",
            "last_name": "Fiette",
            "first_name": "Fabien",
            "id_promo": "1",
            "id_speciality": "1",
            "id": "x771440307e085d7dee3c",
            "email": "fabienfiette@hotmail.fr",
            "gender": true,
            "age": "20"
        }
    ]

### POST new user

`POST url?table=school-manager-users`
##### Body :
    {
        "password": string,
        "last_name": string,
        "first_name": string,
        "id_promo": string,
        "id_speciality": string,
        "email": string,
        "gender": boolean,
        "age": int,
        "id": string
    }
#### Response

    Date: Wed, 11 May 2022 16:50:27 GMT
    Status: 200 OK
    Connection: keep-alive
    Content-Type: application/json
    Content-Length: 25

    {
        "data": {},
        "status": "ok"
    }

### PUT user

`PUT url?table=school-manager-users`
##### Body :
    {
        "password": string,
        "last_name": string,
        "first_name": string,
        "id_promo": string,
        "id_speciality": string,
        "email": string,
        "gender": boolean,
        "age": int,
        "id": string
    }

#### Response

    Date: Wed, 11 May 2022 16:50:27 GMT
    Status: 200 OK
    Connection: keep-alive
    Content-Type: application/json
    Content-Length: 25

    {
        "data": {},
        "status": "ok"
    }

### POST new user

`DELETE url?table=school-manager-users&{column_name}={condition}`

###### Example : `DELETE url?table=school-manager-users&id=k7eb2c3e32d91cfa2`
#### Response

    Date: Wed, 11 May 2022 16:50:27 GMT
    Status: 200 OK
    Connection: keep-alive
    Content-Type: application/json
    Content-Length: 2

    {}
