/* eslint-disable no-console */
/* eslint-disable no-plusplus */
export default class SignUp {
  constructor(url) {
    this.urlApi = url;
    this.domApp = document.getElementById('app');
  }

  getForm = () => {
    // Add a event listener on submit
    const formSignIn = document.getElementById('formSignIn');

    formSignIn.addEventListener('submit', (e) => {
      e.preventDefault();
      // Body for get
      const body = {};
      // Get data from input
      const inputList = e.target.getElementsByClassName('getformSignIn');
      for (let i = 0; i < inputList.length; i++) {
        body[inputList[i].name] = inputList[i].value;
      }
      // Get form
      fetch(`${this.urlApi}?table=school-manager-users&email=${body.email}&password=${body.password}`)
        .then((res) => res.json())
        .then((res) => {
          if (res.status === 'ok') {
            localStorage.setItem('account', JSON.stringify(res.account));
            window.location.href = '/classes';
          } else if (res.status === 'error') {
            console.log('Failed to login');
          }
        });
    });
  };

  render = () => {
    this.domApp.innerHTML += `
  <div class="signup-form">
    <form id="formSignIn" method="get">
      <h2>Sign In</h2>
      <p>Please fill in this form to connect to your account!</p>
      <hr>
        <div class="form-group">
          <input type="email" class="form-control getformSignIn" name="email" placeholder="Email" required="required">
        </div>
      <div class="form-group">
              <input type="password" class="form-control getformSignIn" name="password" placeholder="Password" required="required">
          </div>
      <div class="form-group d-grid gap-2 col-6 mx-auto">
            <button type="submit" class="btn btn-primary">Sign In</button>
        </div>
    </form>
  <div class="hint-text text-center">Don't have an account? <a href="/">Sign-up here</a></div>
</div>
  `;
    this.getForm();
  };
}
