/* eslint-disable no-console */
/* eslint-disable no-plusplus */
export default class Classes {
  constructor(url) {
    this.urlApi = url;
    this.domApp = document.getElementById('app');
  }

  displayStudents(res) {
    document.getElementsByTagName('thead')[0].innerHTML = `
      <tr>
        <th scope="col">#</th>
        <th scope="col">First Name</th>
        <th scope="col">Last Name</th>
      </tr>
    `;

    const tbodyClasses = document.getElementById('tbodyClasses');
    tbodyClasses.innerHTML = '';

    res.forEach((ele, index) => {
      tbodyClasses.innerHTML += `
        <tr id=${ele.id}>
          <td scope="row">${index + 1}</td>
          <td>${ele.first_name}</td>
          <td>${ele.last_name}</td>
        </tr>
      `;
    });
    // Add an onClick on each student
    this.eventListenerClasse('id');
  }

  eventListenerClasse(key) {
    const tbodyClasses = document.getElementById('tbodyClasses');
    const trClasse = tbodyClasses.getElementsByTagName('tr');

    for (let i = 0; i < trClasse.length; i++) {
      trClasse[i].addEventListener('click', (e) => {
        fetch(`${this.urlApi}?table=school-manager-users&${key}=${e.target.parentElement.id}`)
          .then((res) => res.json())
          .then((res) => {
            if (key === 'id_promo') {
              this.displayStudents(res);
            } else if (key === 'id') {
              localStorage.setItem('student', JSON.stringify(res));
              window.location.href = '/student';
            }
          });
      });
    }
  }

  getClasses = () => {
    // Get classe and inner html then
    fetch(`${this.urlApi}?table=school-manager-promotions`)
      .then((res) => res.json())
      .then((res) => {
        const tbodyClasses = document.getElementById('tbodyClasses');

        res.forEach((ele, index) => {
          tbodyClasses.innerHTML += `
            <tr id=${ele.id}>
              <td scope="row">${index + 1}</td>
              <td>${ele.name}</td>
            </tr>
          `;
        });
        // Add an onClick on each classe
        this.eventListenerClasse('id_promo');
      });
  };

  render = () => {
    this.domApp.innerHTML += `
    <table class="table table-dark table-hover">
      <thead>
        <tr>
          <th scope="col">#</th>
          <th scope="col">Promotions</th>
        </tr>
      </thead>
      <tbody id="tbodyClasses">
      </tbody>
    </table>
  `;
    this.getClasses();
  };
}
