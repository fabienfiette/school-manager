/* eslint-disable no-console */
/* eslint-disable no-plusplus */
export default class SignUp {
  constructor(url) {
    this.urlApi = url;
    this.domApp = document.getElementById('app');
  }

  updateSelect = () => {
    // Fetch promotions
    fetch(`${this.urlApi}?table=school-manager-promotions`)
      .then((res) => res.json())
      .then((res) => {
        const selectPromo = document.getElementById('selectPromotion');
        res.forEach((ele) => {
          selectPromo.innerHTML += `
              <option value=${ele.id}>${ele.name}</option>
            `;
        });
      });
    // Fetch specialities
    fetch(`${this.urlApi}?table=school-manager-specialities`)
      .then((res) => res.json())
      .then((res) => {
        const selectSpecia = document.getElementById('selectSpeciality');
        res.forEach((ele) => {
          selectSpecia.innerHTML += `
                <option value=${ele.id}>${ele.name}</option>
              `;
        });
      });
  };

  postForm = () => {
    // Add a event listener on submit
    const formSignUp = document.getElementById('formSignUp');

    formSignUp.addEventListener('submit', (e) => {
      e.preventDefault();
      // Body for post
      const body = {};
      // Generate a uuid
      const uuid = String.fromCharCode(Math.floor(Math.random() * 26) + 97)
        + Math.random().toString(16).slice(2)
        + Date.now().toString(16).slice(4);
      body.id = uuid;
      // Get data from input
      const inputList = e.target.getElementsByClassName('getFormSignUp');
      for (let i = 0; i < inputList.length; i++) {
        if (inputList[i].type === 'radio' && inputList[i].value === 'Male') {
          body[inputList[i].name] = inputList[i].checked;
        } else if (inputList[i].value === 'Female' || inputList[i].name === 'confirm_password') {
          // eslint-disable-next-line no-continue
          continue;
        } else {
          body[inputList[i].name] = inputList[i].value;
        }
      }
      // Generate a Header
      const myHeader = new Headers();
      myHeader.append('Content-Type', 'application/x-www-form-urlencoded');
      myHeader.append('x-api-key', 'RTM8dl1z82ab066OIQ4087FJeY1GIIuE65P7kJ5Q');
      // Post form
      (async () => {
        const rawResponse = await fetch(`${this.urlApi}?table=school-manager-users`, {
          method: 'POST',
          headers: myHeader,
          body: JSON.stringify(body)
        });
        const content = await rawResponse.json();

        if (content.status === 'ok') {
          window.location.href = '/signin';
        }
      })();
    });
  };

  render = () => {
    this.domApp.innerHTML += `
  <div class="signup-form">
    <form id="formSignUp" method="post">
      <h2>Sign Up</h2>
      <p>Please fill in this form to create an account!</p>
      <hr>
        <div class="form-group">
          <label class="form-check-label form-check-inline">Gender</label>
          <div class="form-check form-check-inline">
            <input class="form-check-input getFormSignUp" type="radio" name="gender" id="inlineRadio1" value="Male" required>
            <label class="form-check-label" for="inlineRadio1">Male</label>
          </div>
          <div class="form-check form-check-inline">
            <input class="form-check-input getFormSignUp" type="radio" name="gender" id="inlineRadio2" value="Female" required>
            <label class="form-check-label" for="inlineRadio2">Female</label>
          </div>
        </div>
        <div class="form-group">
          <div class="row">
            <div class="col"><input type="text" class="form-control getFormSignUp" name="first_name" placeholder="First Name" required="required"></div>
            <div class="col"><input type="text" class="form-control getFormSignUp" name="last_name" placeholder="Last Name" required="required"></div>
          </div>
        </div>
        <div class="form-group">
          <input type="email" class="form-control getFormSignUp" name="email" placeholder="Email" required="required">
        </div>
        <div class="form-group">
          <input type="number" step="." class="form-control getFormSignUp" name="age" placeholder="Age" required="required">
        </div>
        <div class="form-group">
          <select class="form-select getFormSignUp" required id="selectPromotion" name="id_promo" aria-label="select promotion">
            <option selected disabled value="">-- Select a promotion --</option>
          </select>
        </div>
        <div class="form-group">
          <select class="form-select getFormSignUp" required id="selectSpeciality" name="id_speciality" aria-label="select speciality">
            <option selected disabled value="">-- Select a speciality --</option>
          </select>
        </div>
      <div class="form-group">
              <input type="password" class="form-control getFormSignUp" name="password" placeholder="Password" required="required">
          </div>
      <div class="form-group">
              <input type="password" class="form-control getFormSignUp" name="confirm_password" placeholder="Confirm Password" required="required">
          </div>        
      <div class="form-group">
        <label class="form-check-label"><input type="checkbox" required="required"> I accept the <a href="#">Terms of Use</a> &amp; <a href="#">Privacy Policy</a></label>
      </div>
      <div class="form-group d-grid gap-2 col-6 mx-auto">
            <button type="submit" class="btn btn-primary">Sign Up</button>
        </div>
    </form>
  <div class="hint-text text-center">Already have an account? <a href="/signin">Login here</a></div>
</div>
  `;
    this.updateSelect();
    this.postForm();
  };
}
