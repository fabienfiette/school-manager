/* eslint-disable no-console */
import './index.scss';
import AppBar from './components/AppBar';
import SignUp from './components/pages/SignUp';
import SignIn from './components/pages/SignIn';
import Classes from './components/pages/Classes';

export default class App {
  constructor() {
    this.urlApi = 'https://gnf7g88xcg.execute-api.eu-west-3.amazonaws.com/school-manager-api/';
    this.domApp = document.getElementById('app');
  }

  getParams = () => {
    if (!window.location.search) {
      return null;
    }
    const urlParams = window.location.search.split('?')[1].split('=');
    const data = {};
    for (let i = 0; i < urlParams.length; i += 2) {
      const counter = i + 1;

      Object.assign(data, { [urlParams[i]]: urlParams[counter] });
    }
    return data;
  };

  getPath() {
    const arrayPath = window.location.pathname.split('/');

    arrayPath.shift();

    console.log(arrayPath.join('/'));

    return {
      path: arrayPath.join('/'),
      params: this.getParams()
    };
  }

  render = () => {
    const { path, params } = this.getPath();
    const appBar = new AppBar(this.urlApi);
    const signUp = new SignUp(this.urlApi);
    const signIn = new SignIn(this.urlApi);
    const classes = new Classes(this.urlApi);
    console.log(path, params);

    switch (path) {
      case 'signin':
        signIn.render();
        break;
      case 'classes':
        appBar.render();
        classes.render();
        break;
      default:
        signUp.render();
    }
  };
}

const app = new App();
app.render();
